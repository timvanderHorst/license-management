# GitLab License management changelog

## v1.2.6

- Fix: better support of go projects (!31)

## v1.2.5

- Feature: support Java 11 via an ENV variable (@haynes !26)

## v1.2.4

- Fix: support multiple MAVEN_CLI_OPTS options (@haynes !27)

## v1.2.3

- Add ability to configure the `mvn install` execution for Maven projects via `MAVEN_CLI_OPTS` (!24)
- Skip `"test"` phase by default when running `mvn install` for Maven projects (!24)

## v1.2.2

- Bump LicenseFinder to 5.6.2

## v1.2.1

- Better support for js npm projects (!14)

## v1.2.0

- Bump LicenseFinder to 5.5.2

## v1.1.0

- Allow `SETUP_CMD` to skip auto-detection of build tool

## v1.0.0

- Initial release
